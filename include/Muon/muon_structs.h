/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <string>

struct region_desc {
  std::string region_name; 
  
  //test
  struct ch1 {
    double x = -756.1;
    double y = 3930.51;
    double z = 142.5;
  } ch1;

  struct ch2 {
    double x = -2258.2;
    double y = 3912.51;
    double z = 58.5;
  } ch2;
};

region_desc reg_test;

class chamber_desc {
  
 public:
  //chamber box sizes
  double chambox_z = 67.0;
  double chambox_y;
  double chambox_x;
  
  //can these be calculated? i.e is the frame always the same thickness? 
  double frame_inner_x;
  double frame_inner_y;  
  
  //number of gas gaps per chamber
  double n_gaps = 4;
  //thickness of single gas gap layer
  double gap_layer_z = 14.06;
  double first_gas_gap_z = -21.09;
  double end_cap_z = 30.385;

  //gap internal layer constants (check how constant these are between chambers!)
  double copper_30mic_size_z = 0.03;
  double g10_gap_size_z = 0.8;
  double poly_gap_size_z = 3.7;
  double gap_layer1_size_z = 3.7;
  double gap_layer5_size_z = 5.0;
  double gap_layer9_size_z = 3.7;

  double copper_endcap_z = 2.25;
  double g10_endcap_z = 1.835;
  double poly_endcap_z = -0.415;
  double copper30mic_layer3_z = -0.4;
  double g10_layer3_z = 0.015; 
  double copper30mic_layer7_z = -1*copper30mic_layer3_z;
  double g10_layer7_z = -1*g10_layer3_z;
  
  double gap_layer1_z = 5.18; 
  double gap_layer3_z = 2.915;
  double gap_layer5_z = 0.;
  double gap_layer7_z = -2.915; 
  double gap_layer9_z = -5.18;

  chamber_desc();
  ~chamber_desc();

};


chamber_desc::chamber_desc(){
  chambox_y = 395.0;
  chambox_x = 1594.0;
  frame_inner_x = 1512.0;
  frame_inner_y = 312.8;
}

chamber_desc::~chamber_desc() {}



// CLass to initialize all the paramaters needed to build a region.
class region_desc_ {

 public:
  //chamber box sizes
  double regbox_x;
  double regbox_y;
  double regbox_z;

};
