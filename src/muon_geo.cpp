/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <DD4hep/DetFactoryHelper.h>
#include <iostream>
#include <string>
#include <Muon/muon_structs.h>

using namespace std;
using namespace dd4hep;


Assembly Chamber(chamber_desc ch, Detector& description) {

  Material copper = description.material("MuonCopper");
  Material g10air = description.material("G10Air");
  Material g10 = description.material("G10");
  Material poly = description.material("MuonPolyurethane");
  Material s_cpc_gas = description.material("S_CPC_gas");


  double chambox_z = ch.chambox_z;
  double chambox_y = ch.chambox_y;
  double chambox_x = ch.chambox_x;

  double frame_inner_x = ch.frame_inner_x;
  double frame_inner_y = ch.frame_inner_y;

  // Assembly in which we will place all volumes
  Assembly chamber_assembly( "chamber_assembly"); 

  //make frame
  Box outer_frame(chambox_x/2, chambox_y/2, chambox_z/2);
  Box inner_frame(frame_inner_x/2, frame_inner_y/2, (chambox_z+0.01)/2);
  SubtractionSolid frame(outer_frame, inner_frame);

  Volume frame_volume( "ChamberFrame", frame, g10air );
  chamber_assembly.placeVolume( frame_volume );

  double gap_layer_x = frame_inner_x;
  double gap_layer_y = frame_inner_y;
  double gap_layer_z = ch.gap_layer_z;
  double end_cap_x = frame_inner_x;
  double end_cap_y = frame_inner_y;

  vector<PlacedVolume> gas_gap_placements; //maybe needed later for sensitive detector stuff
  
  //internal gas gap layers
  Volume copper_30mic("Copper30mic", Box(gap_layer_x/2., gap_layer_y/2., ch.copper_30mic_size_z/2.), copper);
  Volume g10_gap("G10Gap", Box(gap_layer_x/2., gap_layer_y/2., ch.g10_gap_size_z/2.), g10); 

  // Define the End Caps as Assembly object and fill them with their final composition.
  Assembly end_cap3("EndCap3");
  Assembly end_cap6("EndCap6");
  Volume poly_gap("PolyGap", Box(end_cap_x/2., end_cap_y/2., ch.poly_gap_size_z/2.), poly);
  end_cap3.placeVolume(copper_30mic, Position(0, 0, ch.copper_endcap_z ));
  end_cap3.placeVolume(g10_gap, Position(0, 0, ch.g10_endcap_z));
  end_cap3.placeVolume(poly_gap, Position(0, 0, ch.poly_endcap_z));

  end_cap6.placeVolume(copper_30mic, Position(0, 0, -1*ch.copper_endcap_z ));
  end_cap6.placeVolume(g10_gap, Position(0, 0, -1*ch.g10_endcap_z));
  end_cap6.placeVolume(poly_gap, Position(0, 0, -1*ch.poly_endcap_z));

  //make the gas gaps and fill with the component layers
  Volume gas_gap("GasGap",  Box(gap_layer_x/2., gap_layer_y/2., gap_layer_z/2. ), description.material("Air"));

  //layer 1 volume
  Volume gap_layer1("GapLayer1", Box(gap_layer_x/2., gap_layer_y/2., ch.gap_layer1_size_z/2.), poly);
  //layer 3 volume
  Assembly gap_layer3("GapLayer3");
  gap_layer3.placeVolume(copper_30mic, Position(0, 0, ch.copper30mic_layer3_z));
  gap_layer3.placeVolume(g10_gap, Position(0, 0, ch.g10_layer3_z));
  //layer 5 volume
  Volume gap_layer5("GapLayer5", Box(gap_layer_x/2., gap_layer_y/2., ch.gap_layer5_size_z/2.), s_cpc_gas); //CHANGE MATERIAL TO GAS AND MAKE SENSITIVE
  //layer 7 volume
  Assembly gap_layer7("GapLayer7");
  gap_layer7.placeVolume(copper_30mic, Position(0, 0, ch.copper30mic_layer7_z));
  gap_layer7.placeVolume(g10_gap, Position(0, 0, ch.g10_layer7_z));
  //layer 9 volume
  Volume gap_layer9("GapLayer9", Box(gap_layer_x/2., gap_layer_y/2., ch.gap_layer9_size_z/2.), poly);
 
  gas_gap.placeVolume(gap_layer1, Position(0, 0, ch.gap_layer1_z));  
  gas_gap.placeVolume(gap_layer3, Position(0, 0, ch.gap_layer3_z));  
  gas_gap.placeVolume(gap_layer5, Position(0, 0, ch.gap_layer5_z));  
  gas_gap.placeVolume(gap_layer7, Position(0, 0, ch.gap_layer7_z));  
  gas_gap.placeVolume(gap_layer9, Position(0, 0, ch.gap_layer9_z));  

  // Place GasGaps in the Chamber Box
  for ( unsigned gap_num = 0; gap_num < 4; ++gap_num ){
    double placement_z_offset = -21.09+(gap_layer_z*gap_num);
    PlacedVolume pv = chamber_assembly.placeVolume( gas_gap, Position(0, 0, placement_z_offset ));   
    pv.addPhysVolID( "gap", gap_num+1 );
    gas_gap_placements.emplace_back( pv );
  }
  
  // Place EndCaps in the Chamber Box
    chamber_assembly.placeVolume(end_cap3, Position(0, 0, ch.end_cap_z));
    chamber_assembly.placeVolume(end_cap6, Position(0, 0, -1*ch.end_cap_z));
  
  return chamber_assembly;
}

Assembly BuildRegion(region_desc rg, Detector& description){
  
  Assembly region(rg.region_name);
  chamber_desc chm5r4;
  region.placeVolume(Chamber(chm5r4, description), Position(rg.ch1.x, rg.ch1.y, rg.ch1.z));
  region.placeVolume(Chamber(chm5r4, description), Position(rg.ch2.x, rg.ch2.y, rg.ch2.z));
  return region;
}

static Ref_t create_muon( Detector& description, xml_h e, SensitiveDetector sens) {


  // Tell DD4hep we have a tracker: this is needed when using DDG4 to run
  // simulations with Geant4
  sens.setType( "tracker" );
  
  // Converting to xml_det_t in order to have useful accessor methods
  // Using them to create the main DetElement for our subdetector
  xml_det_t  x_det    = e;
  string     det_name = x_det.nameStr();
  DetElement muon_det( det_name, x_det.id() );
 
  string tag = "include";

  for( xml_coll_t c(x_det, Unicode(tag)); c; ++c ) {
    string ref = c.attr<string>(_U(ref));
    unique_ptr<xml::DocumentHolder> doc(new xml::DocumentHolder(xml::DocumentHandler().load(x_det, c.attr_value(_U(ref)))));
    xml_h regions = doc->root();
   
  

 /* 
    for( xml_coll_t reg(regions, Unicode("regionside")); reg; ++reg ) {
      for( xml_coll_t ch(reg, Unicode("muchamber")); ch; ++ch ) {
        xml_dim_t pos = ch.child(_U(position)); 
        cout << pos.x() << endl;
        cout << pos.y() << endl;
        cout << pos.z() << endl;
      }
    }
    */   
 
  }
  //xml_coll_t testxml(e, _U( lvR1ASide ));
//  Assembly chamber_assembly = Chamber(chm5r4, description);
  Assembly region = BuildRegion(reg_test, description);
  Volume       motherVol = description.pickMotherVolume( muon_det );
  //PlacedVolume phv       = motherVol.placeVolume( chamber_assembly, Position( 0, 0, 0 ) );
  PlacedVolume phv       = motherVol.placeVolume( region, Position( 0, 0, 0 ) );
  phv.addPhysVolID( "system", x_det.id() );
  muon_det.setPlacement( phv );
  return muon_det;
}
DECLARE_DETELEMENT( Muon, create_muon )
