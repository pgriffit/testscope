/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <memory>
#include <mutex>

#include "TString.h"
#include "TSystem.h"

#include "TTimeStamp.h"

#include "Conditions/ConditionsRepository.h"

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsMap.h"
#include "DD4hep/ConditionsPrinter.h"
#include "DD4hep/Printout.h"
#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsDataLoader.h"
#include "DDCond/ConditionsIOVPool.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsManagerObject.h"
#include "DDCond/ConditionsSlice.h"
#include "Detector/Common/DeAlignmentCall.h"
#include "Detector/Common/DeConditionCall.h"
#include "Detector/Common/Keys.h"

#include "Detector/Scope/ScopeService.h"


namespace scope {

  void ScopeService::initialize( bool load_conditions ) {

    /// Load the actual description from Detector
    // We use ROOT to expand the variables in the path as it has a convenient
    // function for that.
    TString descriptionFile( m_detDescLocation );
    gSystem->ExpandPathName( descriptionFile );
    const char* fname = descriptionFile.Data();
    dd4hep::printout( dd4hep::INFO, "ScopeService", "Loading DD4hep Geometry: %s", fname );
    m_description.apply( "DD4hep_CompactLoader", 1, (char**)&fname );

    if ( load_conditions ) {
      dd4hep::printout( dd4hep::INFO, "ScopeService", "Initializing the ConditionsManager " );
      TString conditionsLocation( "file://${SCOPE_ROOT}/data/" );
      gSystem->ExpandPathName( conditionsLocation );
      dd4hep::printout( dd4hep::INFO, "ScopeService", "Using conditions location: %s", conditionsLocation.Data() );
      DetectorDataService::initialize( conditionsLocation.Data() );
    }
  }
} // namespace scope
