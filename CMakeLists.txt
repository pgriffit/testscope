###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
cmake_minimum_required(VERSION 3.12 FATAL_ERROR)

project(scope)

#==========================================================================
set(CMAKE_CXX_STANDARD 17)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
find_package(DD4hep REQUIRED DDCore )
find_package(ROOT REQUIRED Geom Core GenVector)
include ( ${DD4hep_DIR}/cmake/DD4hep.cmake )
include ( ${DD4hep_DIR}/cmake/DD4hepBuild.cmake )
dd4hep_configure_output()
dd4hep_set_compiler_flags()

#==========================================================================
if(TARGET XercesC::XercesC)
  SET(OPT_XERCESC XercesC::XercesC)
endif()

#==========================================================================
# Common tools
file(GLOB ScopeCommon_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/src_common/Common/*.cpp	
  ${CMAKE_CURRENT_SOURCE_DIR}/src_common/Conditions/*.cpp)

add_library(ScopeCommon SHARED ${ScopeCommon_SOURCES} )
target_include_directories(ScopeCommon PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/>
  $<INSTALL_INTERFACE:include>  # <prefix>/include
  ${XercesC_INCLUDE_DIRS})
target_link_libraries(ScopeCommon ROOT::Core ROOT::Geom ROOT::GenVector 
  DD4hep::DDCore DD4hep::DDCond DD4hep::DDAlign ${OPT_XERCESC} )

#==========================================================================
add_library(ScopeSvc SHARED ${PROJECT_SOURCE_DIR}/src_det/DeScope.cpp 
        ${PROJECT_SOURCE_DIR}/src_det/ScopeService.cpp 
        ${PROJECT_SOURCE_DIR}/src_det/DeScopeConditionCalls.cpp
        ${PROJECT_SOURCE_DIR}/src_det/DeScopeHandles.cpp)
target_include_directories(ScopeSvc PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include/>
  $<INSTALL_INTERFACE:${PROJECT_SOURCE_DIR}/include>  
  ${XercesC_INCLUDE_DIRS})
target_link_libraries(ScopeSvc ScopeCommon ROOT::Core ROOT::Geom ROOT::GenVector 
  DD4hep::DDCore DD4hep::DDCond DD4hep::DDAlign ${OPT_XERCESC} )

#==========================================================================
add_executable(test_scan ${PROJECT_SOURCE_DIR}/src_det/test_scan.cpp)
target_include_directories(test_scan PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include/>
  $<INSTALL_INTERFACE:include>  
  ${XercesC_INCLUDE_DIRS})
target_link_libraries(test_scan ScopeSvc ROOT::Core ROOT::Geom ROOT::GenVector ${OPT_XERCESC}
  DD4hep::DDCore DD4hep::DDCond DD4hep::DDAlign)

  add_executable(test_conditions ${PROJECT_SOURCE_DIR}/src_det/test_conditions.cpp)
  target_include_directories(test_conditions PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include/>
    $<INSTALL_INTERFACE:include>  
    ${XercesC_INCLUDE_DIRS})
  target_link_libraries(test_conditions ScopeSvc ScopeCommon ROOT::Core ROOT::Geom ROOT::GenVector ${OPT_XERCESC}
    DD4hep::DDCore DD4hep::DDCond DD4hep::DDAlign)
  

#==========================================================================
# Plugin to create the geometry 
dd4hep_add_plugin(ScopePlugins SOURCES src/*.cpp
       USES ScopeCommon ScopeSvc DD4hep::DDCore DD4hep::DDCond DD4hep::DDAlign
       ROOT::Core ROOT::Geom ROOT::GenVector ${OPT_XERCESC}
       INCLUDES ${GitCondDB_INCLUDE_DIR} ${DD4hep_INCLUDE_DIRS} 
       ${XercesC_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})


#==========================================================================
install(TARGETS ScopePlugins DESTINATION lib)
install(TARGETS ScopeCommon DESTINATION lib)
install(TARGETS ScopeSvc DESTINATION lib)
install(DIRECTORY compact DESTINATION ${CMAKE_INSTALL_PREFIX})
install(DIRECTORY data DESTINATION ${CMAKE_INSTALL_PREFIX})

install(TARGETS test_scan DESTINATION bin)
install(TARGETS test_conditions DESTINATION bin)

file( GLOB INST_SCRIPTS scripts/* )
install(FILES ${INST_SCRIPTS} PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
GROUP_EXECUTE GROUP_READ WORLD_EXECUTE WORLD_READ DESTINATION bin)

#==========================================================================
dd4hep_configure_scripts(scope DEFAULT_SETUP WITH_TESTS)

configure_file(${CMAKE_SOURCE_DIR}/cmake/setenv.sh.in setenv.sh @ONLY)
install(FILES ${CMAKE_BINARY_DIR}/setenv.sh DESTINATION bin)
