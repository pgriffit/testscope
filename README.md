Development environment for Muon detector in DD4Hep, based off of testscope.
==================================

Introduction
------------

Requirements:
  - CMake
  - ROOT
  - Geant4
  - DD4hep (>v1r11)

Content
-------

 *compact/muon.xml* compact description file for the muon detector.
 
 *compact/muon/* additional xml includes for muon 

 Some scripts allow displaying the Geometry and running Geant4 against it.
   - *show_ideal*: Display the ideal geometry
   - *show_misaligned*: Display the  geometry with a global misalignment as defined in *global_alignment.xml*
   

Building and Running
--------------------

To checkout, build and install
```
git clone ssh://git@gitlab.cern.ch:7999/pgriffit/testscope.git
mkdir build && cd build
```

on lxplus7, the necessary depedencies (DD4hep, ROOT, Xerces...) can be setup by using:
```
source ../testscope/lxplus/setenv.sh
```



```
cmake -DCMAKE_INSTALL_PREFIX=../install ../testscope
make install
```

To setup the environment:
```
source ../install/bin/setenv.sh
```

Then for example:
```
geoDisplay -input testscope/compact/muon.xml
```

Example tools
--------------------
  - show_ideal.sh: opens a windo that display the ideal placements of the sensors
  - show_misaligned: Uses a global misalignment file (in DD4hep format) to display the detector, misaligned
  - test_scan: Loads the geometry and uses a few lines of ROOT to find the materials encoutered on a given path
  - test_conditions: tries loading the condition in LHCb, and loads the Detector element


Tips & tricks
-------------

To dump the position of the volumes:
``` 
geoPluginRun -interpreter -input compact/muon.xml -volmgr -plugin DD4hep_DetectorVolumeDump -posi 
```

To check for overlaps:
``` 
checkOverlaps --compact compact/muon.xml
```

